﻿using Microsoft.AspNetCore.Authorization;

namespace FortCode.Filters
{
    public class BasicAuthorizationAttribute : AuthorizeAttribute
    {
        public BasicAuthorizationAttribute()
        {
            Policy = "BasicAuthentication";
        }
    }
}
