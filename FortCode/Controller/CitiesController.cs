﻿using FortCode.Models;
using FortCode.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using System.Linq;
using System;

namespace FortCode.Controller
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CitiesController : ControllerBase
    {
        private readonly TestDBContext testDBContext;

        public CitiesController(TestDBContext _testDBContext)
        {
            testDBContext = _testDBContext;
        }

        // GET: api/<CitiesController>
        [HttpGet]
        public ActionResult<string> GetFavouriteCities()
        {            
            var favCities = testDBContext.Cities.Where(c => c.UserId == (Convert.ToInt16(HttpContext.User.Identity.Name)));
            if (favCities == null)
            {
                return NoContent();
            }
            return Ok(favCities.Select(w => new FavCityViewModel { CityName = w.CityName }).ToList());

        }

        // POST api/<CitiesController>
        [HttpPost("{city}/{country}")]
        public ActionResult AddFavoutieCity(string city, string country)
        {            
            City cityFound = testDBContext.Cities.FirstOrDefault(c => (c.CityName.Equals(city)) && (c.Country.Equals(country)) && (c.UserId == (Convert.ToInt16(HttpContext.User.Identity.Name))));
            if (cityFound == null)
            {
                testDBContext.Cities.Add(new City() { CityName = city, Country = country, UserId = Convert.ToInt16(HttpContext.User.Identity.Name) });
                testDBContext.SaveChanges();
                return Ok("Success");
            }
            return BadRequest("City is already added as favourite");
        }

        // DELETE api/<CitiesController>/5
        [HttpDelete("{city}/{country}")]
        public ActionResult DeleteCity(string city, string country)
        {            
            City cityToDel = testDBContext.Cities.FirstOrDefault(c => (c.CityName.Equals(city)) && (c.Country.Equals(country)) && (c.UserId == (Convert.ToInt16(HttpContext.User.Identity.Name))));
            if (!(cityToDel == null))
            {
                testDBContext.Cities.Remove(cityToDel);
                testDBContext.SaveChanges();
                return Ok("success");
            }
            return BadRequest("City not found to delete");
        }
    }
}
