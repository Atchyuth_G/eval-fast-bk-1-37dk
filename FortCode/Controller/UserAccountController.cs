﻿using FortCode.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace FortCode.Controller
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserAccountController : ControllerBase
    {
        public IConfiguration _configuration;
        private TestDBContext dbContext;
        public UserAccountController(TestDBContext _testDBContext, IConfiguration config)
        {
            _configuration = config;
            dbContext = _testDBContext;
        }

        // POST api/<UserController>
        [HttpPost]
        public ActionResult RegisterUser([FromBody] UserAccount user)
        {
            if (dbContext.UserAccounts.FirstOrDefault(c => c.Email == user.Email) == null)
            {
                dbContext.UserAccounts.Add(user);
                dbContext.SaveChanges();
                Response.Cookies.Append("Email", string.Empty);
                return Ok("Success");
            }
            return BadRequest("User already exists");
        }
    }
}
