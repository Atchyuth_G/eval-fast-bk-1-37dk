﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FortCode.Models
{
    public partial class City
    {
        public int Id { get; set; }
        public string CityName { get; set; }
        public string Country { get; set; }
        public int? UserId { get; set; }

        public virtual UserAccount User { get; set; }
    }
}
