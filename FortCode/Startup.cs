using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using FortCode.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authentication;
using FortCode.Filters;
using FortCode.Services;

namespace FortCode
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public virtual void ConfigureServices(IServiceCollection services)
        {
            services
                .AddMvc();

            services
                .AddControllers();

            services.AddEntityFrameworkSqlServer()
            .AddDbContext<TestDBContext>
            (option => option.UseSqlServer(Configuration["ConnectionStrings:DefaultConnection"]));

            services.AddAuthentication("BasicAuthentication")
    .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>("BasicAuthentication", null);

            services.AddScoped<ILoginService, LoginService>();

            services.AddScoped(typeof(ILoginService), typeof(LoginService));

            services.AddScoped<IAuthenticatedUser>(_ => new AuthenticatedUser("", false, ""));
        }

        public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app
                .UseFileServer()
                .UseRouting()
                .UseAuthentication()
                .UseAuthorization()
                .UseEndpoints(endPoints => { endPoints.MapControllers(); });            
        }
    }
}
