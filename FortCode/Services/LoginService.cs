﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FortCode.Models;

namespace FortCode.Services
{
    public class LoginService : ILoginService
    {
        private TestDBContext _dbContext;
        public LoginService(TestDBContext dBContext)
        {
            _dbContext = dBContext;
        }

        public UserAccount Authenticate(string userName, string pwd)
        {
            return _dbContext.UserAccounts.FirstOrDefault(u => u.Email == userName && u.Password == pwd);
        }
    }
}
